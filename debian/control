Source: khelpcenter
Section: doc
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libgumbo-dev,
               libxapian-dev,
               libxml2-dev,
               kf6-extra-cmake-modules,
               kf6-karchive-dev,
               kf6-kbookmarks-dev,
               kf6-kcolorscheme-dev,
               kf6-kconfig-dev,
               kf6-kcoreaddons-dev,
               kf6-kdbusaddons-dev,
               kf6-kdoctools-dev,
               kf6-ki18n-dev,
               kf6-kio-dev,
               kf6-kservice-dev,
               kf6-ktexttemplate-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kwindowsystem-dev,
               kf6-kxmlgui-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-webengine-dev
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/kde/workspace/khelpcenter
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/khelpcenter
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/khelpcenter.git

Package: khelpcenter
Architecture: any
Depends: kdoctools6, ${misc:Depends}, ${perl:Depends}, ${shlibs:Depends}
Recommends: man-db
Description: KDE documentation viewer
 KHelpCenter uses meta data files which describe the documentation
 available in the system. Each document is represented by a meta data
 file and shown as an entry in the KHelpCenter navigation tree view.
 The meta data contains information about title and short description
 of the document, the location of the document and some more
 information like how to search the document and translations of title
 and description. Document hierarchy is represented as hierarchy of
 the meta data files. Directories are also described by a meta data
 file which contains the same information as a document meta data
 file.
